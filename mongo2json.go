package main
 
import (
    gonode "github.com/jgranstrom/gonodepkg"
    json "github.com/jgranstrom/go-simplejson"
	"gopkg.in/mgo.v2"
	// "reflect"
)
 
func main() {	
    gonode.Start(process)
}
 
func process(cmd *json.Json) (response *json.Json) {	
    response, m, _ := json.MakeMap()
 
    session, err := mgo.Dial("mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge")
	if err != nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	// Collection
	var results []string

	//*******This is where the issue was***********
    err = session.DB("dev-challenge").C("").Find(nil).All(&results)
    if err != nil {
        panic(err)
    }
    if results == nil {
    	m["responseText"] = "Null"
	}else{
		m["responseText"] = results
	}
    
    // fmt.Println("Results All: ", results) 

	// Index
	// index := mgo.Index{
	// 	Key:        []string{"name", "phone"},
	// 	Unique:     true,
	// 	DropDups:   true,
	// 	Background: true,
	// 	Sparse:     true,
	// }

	// err = c.EnsureIndex(index)
	// if err != nil {
	// 	panic(err)
	// }

    return 
}